#include "timer_msp432.h"
#include "time.h"

unsigned long long uptime = 0;
void tick(void * arg){
    uptime++;
}


void InitializeTimer() {
    timer_msp432 * Timer = new timer_msp432();
    Timer->setCallback(tick, nullptr);
    Timer->setPeriod(1000, TIMER::PERIODIC);
    Timer->start();
}

void delay(unsigned long long ms){
    unsigned long long now = uptime;

    while(1){
        unsigned long long diff = uptime - now ;
        if(diff <= ms) {
            break;
        }
    }
}
