#include <math.h>
#include "gpio_msp432.h"
#include "time.h"
class Stepper {
public:
       Stepper(int _maxSteps, int stepper_pin_1, int stepper_pin_2, int stepper_pin_3) {
           this->enable_pin = new gpio_msp432_pin(stepper_pin_1);
           this->step_pin = new gpio_msp432_pin(stepper_pin_2);
           this->direction_pin = new gpio_msp432_pin(stepper_pin_3);

           this->enable_pin->gpioMode(GPIO::OUTPUT);
           this->step_pin->gpioMode(GPIO::OUTPUT);
           this->direction_pin->gpioMode(GPIO::OUTPUT);

           this->enable_pin->gpioWrite(HIGH); //disable stepper
           this->led_blue->gpioMode(GPIO::OUTPUT);

           this->maxSteps = _maxSteps;
       }

       void setSpeed(int _speed){
          this->speed = speed;
       }

       void step(int steps) {
           start();
           if(abs(steps)>steps){
                this->direction_pin->gpioWrite(LOW);
            } else {
               this->direction_pin->gpioWrite(HIGH);
           }
           for(int i = 0; i < steps; ++i ) {
                this->step_pin->gpioToggle();
                if(abs(steps)>steps) {
                    this->stepCounter--;
                }
                else {
                    this->stepCounter++;
                }
                this->led_blue->gpioToggle();
                delay(this->speed);
           }
           stop();
       }
       void start() {
           this->enable_pin->gpioWrite(LOW);
       }
       void stop() {
           this->enable_pin->gpioWrite(HIGH);
       }
       int stepCounter = 0;
   private:
       int maxSteps = 200; //Standard

       gpio_msp432_pin * enable_pin;
       gpio_msp432_pin * step_pin;
       gpio_msp432_pin * direction_pin;

       gpio_msp432_pin * led_blue = new gpio_msp432_pin(PORT_PIN(2,2));
       int speed = 10;

};
