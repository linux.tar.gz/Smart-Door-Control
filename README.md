# Smart-Door-Control
this Repo is about Controlling A Door(or something like that) with multiple Sensors.
So you hit a Button or touch a Sensor or authenticate with RFID and the Door opens. MAGIC!!!!!
RFID needs more time :(


The master branch is a template for implementation the Sensors.
Each Branch covers the implementation with a Sensor.

Branch | Description | Works?
--- | --- | ---
RFID | Control your Door with a NFC Chip | NO, NEEDS MORE TIME
ExternTouch | Control the Door with the KY touch modul from the kit | YEAH
MSP432Button | Intern button opens it | YEAH
ExternButton | Control it with an Pullup Button | YEAH
PIR | opens the door if you move | YEAH
CAPIO | touch with msp pins | NO, Second MSP required

# Getting started
There are two ways to use this repo
1. With a Docker image
    docker run docker run --rm --privileged -it --device=/dev/usb/hiddev0 --device=/dev/hidraw1 phalanx8/smart-door-control
2. or the boring way with just cloning it
