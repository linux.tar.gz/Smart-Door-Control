class Sensor {
public:
    virtual bool readSensor() = 0;
};
