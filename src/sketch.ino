#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>

#define WIFI_SSID "IoT"
#define WIFI_PW "bPxnPKkmbGyM9Y8TkSRpHg=="

#define MQTT_HOST "t1000"
#define MQTT_PORT 1883
#define MQTT_USER "iot"
#define MQTT_PW "iot"

#define BUFFERSIZE 1024

String mqttPrefix;
WiFiClient wifiClient;
PubSubClient mqtt(wifiClient);


void blink(uint8_t times = 1, unsigned int sleep = 250) {
  for (uint8_t i = 0; i < times; i++) {
      digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      delay(sleep);
      digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      delay(sleep);
  }
}

void mqttCallback(char* topic, byte* payload, size_t length) {
  // TODO
}

void setupWifi() {
  // ensure reconnection
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);

  WiFi.mode(WIFI_STA);
  Serial.printf("Connecting to %s ", WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PW);
  
  for (int i = 0; i < 20 && (WiFi.status() != WL_CONNECTED); i++) {
    blink();
    Serial.print(".");
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.print("Connected with IP: ");
    Serial.println(WiFi.localIP());
  } else {
    Serial.println("Error connecting.");
    blink(3, 100);
  }
}

void setupMqtt() {
  mqtt.setServer(MQTT_HOST, MQTT_PORT);
  mqtt.setCallback(mqttCallback);
  connectMqtt();
}

bool connectMqtt() {
  if (!mqtt.connected()) {
    Serial.println("Attempting MQTT connection...");
    if (mqtt.connect(String(ESP.getChipId()).c_str(), MQTT_USER, MQTT_PW)) {
      Serial.println("connected");      
      mqtt.publish((mqttPrefix + "msg").c_str(), "connected");
      mqtt.subscribe((mqttPrefix + "cmd/+").c_str());
    } else {
      Serial.printf("failed: %d\n", mqtt.state());
      return false;
    }
  }
  return true;
}

long lastConnected = 0;
void connect() {
  long now = millis();
  unsigned int connectTimeout = 5000; // ms

  if (now - lastConnected < connectTimeout)
   return;  // dont spam

  lastConnected = now;

  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("WLAN: Not connected!");
    blink(3, 100);
    return;
  }
  
  if (!connectMqtt()) {
    Serial.println("MQTT: Not connected!");
    blink(2, 100);
  }
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // LOW = on
  
  Serial.begin(115200);
  Serial.println();
  
  // Setup WiFi
  setupWifi();
  
  // Setup MQTT
  setupMqtt();
}

char buffer[BUFFERSIZE];
void processSerial() {
  if (Serial.readBytesUntil('\n', buffer, BUFFERSIZE)) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    String line = String(buffer);
    line.trim();
    int tabi = line.indexOf('\t');
    if (tabi > 0) {
      String topic = line.substring(0, tabi);
      String message = line.substring(tabi + 1, line.length());
      mqtt.publish(topic.c_str(), message.c_str());
    } else {
      Serial.println(F("ERR: no \\t."));
    }
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
}

void loop() {
  connect();
  mqtt.loop();
  processSerial();
  delay(5); // yield & save energy
}
