#include "Stepper.cpp"
class StepperController {
public:
    Stepper * stepper  = new Stepper(latch, PORT_PIN(2,7),PORT_PIN(2,6),PORT_PIN(2,5));
    virtual void eject() = 0;
    virtual void fullEject(){
        stepper->step(this->latch - stepper->stepCounter);
    }
    virtual void interEject(){
        stepper->step(-(this->latch - stepper->stepCounter));
    }
    virtual void controlStepper() = 0;

    int latch = 200; // 200 * 1.8 Grad
};
